package com.squaredevops.loginsystemc524;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;

import java.util.ArrayList;

public class UserList extends AppCompatActivity {

    /*
        LIST VIEW ACTIVITY
     */

    ListView listView;
    DatabaseHelper databaseHelper;
    SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_list);
        setTitle("User List");

        databaseHelper = new DatabaseHelper(this);
        listView = (ListView) findViewById(R.id.listView_id);

        // Lookup the swipe container view
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeContainer);

        // Setup refresh listener which triggers new data loading
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Your code to refresh the list here.
                // Make sure you call swipeContainer.setRefreshing(false)
                // once the network request has completed successfully.
                toastMessage("Refresh");
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        swipeRefreshLayout.setRefreshing(false); // true = infinity refresh
                        // you must recall (again) the method here for refresh
                        prepareListView(); // for reload user list method
                    }
                }, 500); // delay time
            }
        });

        // Configure the refreshing colors
        swipeRefreshLayout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        prepareListView();

        // adding back button
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    // action button
    public boolean onOptionsItemSelected(MenuItem item){

        // home ~ this will redirect to home activity

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void prepareListView() {

        // calling method
        Cursor cursor = databaseHelper.displayData();
        ArrayList<String> listData = new ArrayList<>();
        while(cursor.moveToNext()){
            // get the value from the database in column 1
            // then add it to the ArrayList
            // index 1 means usernames
            listData.add(cursor.getString(1));
            //listData.add(cursor.getString(2));
        }

        // create the list adapter and set the adapter
        ListAdapter listAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, listData);
        listView.setAdapter(listAdapter);

        // set an onItemClickListener to the ListView
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                /*
                // int i - is id number
                toastMessage("Clicked " + i); // long l - is id number

                // new activity
                Intent intent = new Intent(UserList.this, ModifyUserList.class);
                startActivity(intent);
                // ALSO YOU CAN IMPLEMENT SOMETHING FOR LIST ITEMS
                */

                // int i - is id number
                String name = adapterView.getItemAtPosition(i).toString();

                // get the id associated with that name
                Cursor cursor = databaseHelper.getItemID(name);

                int itemID = -1;
                while(cursor.moveToNext()){
                    // index 0 means id
                    itemID = cursor.getInt(0);
                }

                if( itemID > -1) {
                    // starting new activity based on id clicked by user
                    Intent intent = new Intent(UserList.this, ModifyUserList.class);
                    intent.putExtra("id", itemID);
                    //intent.putExtra("Name", name); // to print data
                    startActivity(intent);
                }
                else {
                    toastMessage("No ID associated with that name");
                }

            }
        });
    }

    private void toastMessage(String message){
        final Toast toast = Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT);
        toast.show();

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                toast.cancel();
            }
        },500); // delay time
    }
}