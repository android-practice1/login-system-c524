package com.squaredevops.loginsystemc524;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class ModifyUserList extends AppCompatActivity {

    EditText username, password;
    Button update, delete;
    int selectedID;
    String selectedName;
    String selectedPass;
    TextView previous_username, previous_password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modify_user_list);
        setTitle("Modify User Data"); // custom actionbar title

        username = (EditText) findViewById(R.id.update_username);
        password = (EditText) findViewById(R.id.update_password);
        update = (Button) findViewById(R.id.update_button);
        delete = (Button) findViewById(R.id.delete_button);
        previous_username = (TextView) findViewById(R.id.previous_data_name);
        previous_password = (TextView) findViewById(R.id.previous_data_password);

        // get the intent extra from the ListDataActivity
        Intent intent = getIntent();

        // now get the itemID we passed as an extra
        selectedID = intent.getIntExtra("id",-1); //NOTE: -1 is just the default value

        //now get the name we passed as an extra
        selectedName = intent.getStringExtra("username");
        selectedPass = intent.getStringExtra("password");

        // set the text to show the current selected name
        username.setText(selectedName);
        password.setText(selectedPass);

        // printing id
        // textView.setText("selected id = " + selectedID);

        DatabaseHelper databaseHelper = new DatabaseHelper(this);
        Cursor cursor = databaseHelper.getDatawithID(selectedID);
        StringBuffer stringBuffer = new StringBuffer();

        // printing username
        if (cursor.moveToFirst()) {
            do {
                stringBuffer.append(cursor.getString(1));
            } while(cursor.moveToNext());
        }
        previous_username.setText(stringBuffer.toString());

        // printing password
        StringBuffer stringBuffer1 = new StringBuffer();
        if (cursor.moveToFirst()) {
            do {
                stringBuffer1.append(cursor.getString(2)).append("\n");
            } while(cursor.moveToNext());
        }
        previous_password.setText(stringBuffer1.toString());

        // update button
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toastMessage("clicked on update button");

                String name = username.getText().toString();
                String pass = password.getText().toString();

                if (!name.equals("")){
                    databaseHelper.updateName(selectedID, name, pass);
                } else{
                    toastMessage("You must enter a name");
                }
            }
        });

        // delete button
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toastMessage("clicked on delete button");
            }
        });

        // adding back button
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

    }

    // action button
    public boolean onOptionsItemSelected(MenuItem item){

        // home ~ this will redirect to home activity

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    // custom toast message
    private void toastMessage(String message){
        final Toast toast = Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT);
        toast.show();

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                toast.cancel();
            }
        },500); // delay time
    }
}