package com.squaredevops.loginsystemc524;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Handler;
import android.widget.Toast;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "database.db"; // database name
    private static final String TABLE_NAME = "login_data"; // table name
    private static final String ID = "id"; // primary key 0
    private static final String NAME = "username"; // column 1
    private static final String PASSWORD = "password"; // column 2

    // if we need to change any column then we need to change version number also
    private static final int VERSION_NUMBER = 1; // version number

    // create table query
    private static final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME +
            "(" + ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "+
            NAME + " VARCHAR(255) NOT NULL, " +
            PASSWORD + " TEXT NOT NULL)";

    // drop table query
    private static final String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;
    private Context context;

    // constructor
    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, VERSION_NUMBER);
        DatabaseHelper.this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        // creating table
        try {
            sqLiteDatabase.execSQL(CREATE_TABLE);
            toastMessage("database table has been created");
        } catch (Exception exception) {
          toastMessage("Exception : " + exception);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        // for change any column then also need to change VERSION_NUMBER
        // for update column we need to drop and then create again
        try {
            // drop table
            sqLiteDatabase.execSQL(DROP_TABLE);
            toastMessage("onUpgrade is called");

            // creating table again
            onCreate(sqLiteDatabase);
        } catch (Exception exception) {
            toastMessage("Exception : " + exception);
        }

    }

    // adding data
    public boolean insertData(String name, String pass) {

        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        // putkey
        contentValues.put(NAME, name);
        contentValues.put(PASSWORD, pass);

        long result = sqLiteDatabase.insert(TABLE_NAME, null, contentValues);

        // if date as inserted incorrectly it will return -1
        if (result == -1) {
            return false;
        } else {
            return true;
        }
    }

    // Fetch data from database
    public Cursor displayData(){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "SELECT * FROM " + TABLE_NAME;
        Cursor cursor = sqLiteDatabase.rawQuery(query, null);
        return cursor;
    }

    // Returns only the ID that matches the name passed in
    public Cursor getItemID(String nameString){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        String query = "SELECT " + ID + " FROM " + TABLE_NAME +
                " WHERE " + NAME + " = '" + nameString + "'";

        Cursor cursor = sqLiteDatabase.rawQuery(query, null);
        return cursor;
    }

    public Cursor getDatawithID(int id) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        //             select * from          user        where     id    =      1;
        String query = "SELECT * FROM " + TABLE_NAME + " WHERE " + ID + " = " + id;
        Cursor cursor = sqLiteDatabase.rawQuery(query, null);
        return cursor;
    }

    public void updateName(int id, String newName, String newPass){

        // multiple items
        // UPDATE salery SET Name = 'Galib', salery = 2800 WHERE id = 2 AND Name = 'Mula';

        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();

        String newQuery = "UPDATE " + TABLE_NAME + " SET " + NAME +
                " = '" + newName + "', " +
                PASSWORD + " = " + newPass + " WHERE " + ID + " = " + id + ";" ;
        sqLiteDatabase.execSQL(newQuery);
    }

    // matching login data
    public Boolean findPassword(String name, String pass) {

        // getting data from database
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM " + TABLE_NAME, null);
        Boolean result = false;

        if (cursor.getCount() == 0) {
            toastMessage("No data found");
        } else {

            while (cursor.moveToNext()) {
                String username = cursor.getString(1); // getting data from column 1 (username)
                String password = cursor.getString(2); // getting data from column 2 (password)

                if (username.equals(name) && password.equals(pass)) {
                    result = true;
                    break;
                }
            }
        }

        return result;
    }

    // custom toast message
    private void toastMessage(String message){
        final Toast toast = Toast.makeText(context, message, Toast.LENGTH_SHORT);
        toast.show();

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                toast.cancel();
            }
        },500); // delay time
    }
}
