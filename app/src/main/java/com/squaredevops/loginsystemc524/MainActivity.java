package com.squaredevops.loginsystemc524;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Button login, register;
    EditText user, pass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        login = findViewById(R.id.login_button);
        register = findViewById(R.id.register_button);
        user = findViewById(R.id.username);
        pass = findViewById(R.id.password);

        // object
        DatabaseHelper databaseHelper = new DatabaseHelper(this );

        // login button
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // getting data from edittext
                String username = user.getText().toString();
                String password = pass.getText().toString();

                Boolean result = databaseHelper.findPassword(username, password);

                // if it matches with true then it will pass to MainPage activity
                if(result == true) {
                    Intent intent = new Intent(MainActivity.this, MainPage.class);
                    startActivity(intent);
                } else {
                    toastMessage("failed to match user data");
                }

            }
        });

        // register button
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // going to register activity
                Intent intent = new Intent(MainActivity.this, RegisterActivity.class);
                startActivity(intent);
            }
        });
    }

    // menu on action bar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    // on menu option
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){

            case R.id.user_list:
                toastMessage("clicked on registered user list");
                startActivity(new Intent(getApplicationContext(), UserList.class));
                break;

            case R.id.about:
                toastMessage("clicked on about information");
                startActivity(new Intent(getApplicationContext(), About.class));
                break;

            default:
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    // custom toast message
    private void toastMessage(String message){
        final Toast toast = Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT);
        toast.show();

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                toast.cancel();
            }
        },500); // delay time
    }
}