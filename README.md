# LoginSystem C524 

Started : 5-10-2021  <br>
Version : 1.12 <br>
Status  : Work in Progress  
> THIS IS A EXPERIMENTAL APPLICATION FOR LEARNING PURPOSES 

**Things I learned from here** 
1. How to register user's data.
2. Show registered user's data in listview. 
3. Modify (update/delete) registered user's data.
4. Match registered user's data for login. 
5. Swipe to refresh in listview. 

**Useful Method**
1. `findpassword` method from database for matching user input with database 
```java 
// matching login data
public Boolean findPassword(String name, String pass) {

    // getting data from database
    SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
    Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM " + TABLE_NAME, null);
    Boolean result = false;

    if (cursor.getCount() == 0) {
        Toast.makeText(context, "No data is found", Toast.LENGTH_SHORT).show();
    } else {

        while (cursor.moveToNext()) {
            String username = cursor.getString(1); // getting data from column 1 (username)
            String password = cursor.getString(2); // getting data from column 2 (password)

            if (username.equals(name) && password.equals(pass)) {
                result = true;
                break;
            }
        }
    }

    return result;
}
```
2. Login Button action click  
```java 
// login button
login.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {

        // getting data from edittext
        String username = user.getText().toString();
        String password = pass.getText().toString();

        Boolean result = databaseHelper.findPassword(username, password);

        // if it matches with true then it will pass to MainPage activity
        if(result == true) {
            Intent intent = new Intent(MainActivity.this, MainPage.class);
            startActivity(intent);
        } else {
            toastMessage("failed to match user data");
        }

    }
});
```

**Screenshot** <br>
![login](login_page.PNG)
![register](register_page.PNG)

**To Do:**
```
*. [done] add table 
*. [done] first time show register activiy (no database == new database)
*. [done] add a actionbar menu items (About, Registered User List)
*. [done] add 'submit' button toast 
*. [done] design about activity 
*. [done] register system
*. [done] login system (match)
*. [done] show login data/information/registered user list
*. [done] new activity for update and delete user list from database
*. [done] add back button in actionbar on modify user list activity  
*. [done] show data in text view/edittext to view previous data
*. [done] print specific user info based on id 
*. [done] swipe to refresh activity in user list activity
*. add delete finctionality 
*. when user press on 'delete' then redirect to 'user list' activity
```
Farhan Sadik
